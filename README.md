# Jenkins Workshop



## Desafio
Tendrán que crear un pipeline que tenga los siguientes stages:


1. Clono el repositorio de código: 
```
https://gitlab.com/jenkinsworkshop99/jenkins-workshop.git
```


2. Docker login a docker.io con las credenciales de docker.io
```
docker login docker.io -u $USERNAME -p $PASSWORD
```


3. Hacer un docker build con el tag nginx-workshop-userX 
```
docker build -t nginx-workshop-userX  .
```

y luego taguear la imagen con para subirla a docker.io
```
docker.io/jenkinsworkshop99/jenkins-workshop:userX 
```


4. Pushear la imagen a  docker.io/jenkinsworkshop99/jenkins-workshop:userX
```
sin ayuda
```

5. Hacer un docker stop y borrado si existiera el mismo nombre de contenedor
```
docker stop nginx-workshop-userX  || true && docker rm nginx-workshop-userX || true
```

6. Hacer un docker run del ngnix
```
docker run -d  --name nginx-workshop-userX  -p 909X:80 nginx-workshop
```

7. Verificar que el contenedor corra correctamente
```
curl localhost:909X
```


## Requisitos previos a la creación del pipeline:
Crear una credencial con Id. jenkins-workshop-gitlab del tipo api token para autenticar con gitlab.  
Crear una credencial con Id. jenkins-workshop-docker del tipo user/password para autenticar con docker.io  


```
pipeline {
    agent any

    stages {
            stage('Clono el repo de codigo') {
            steps {
                        git branch: 'main',
                            credentialsId: 'jenkins-workshop-gitlab',
                            url: 'https://gitlab.com/completar'
            
                        sh "ls -lat"

            }

        }

        stage('docker login') {
            steps { 
                    withCredentials([usernamePassword(credentialsId: 'jenkins-workshop-docker', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
                   
                         }       
                    }           
              }
        stage('Hago un docker build') {
            steps {
                   
            }

        }
        stage('Hago un docker push') {
            steps {
                   
                 
            }

        }
        stage('Hago un docker run') {
            steps {
                  
            }

        }
        stage('Verificar que el contenedor corra correctamente') {
            steps {
                   

            }

        }
    }    
}

```
